import java.util.Scanner;

public class Main {
    private static final int EXIT = 0;

    private Hotel hotel;
    private Person person = new Person();

    private Main() {
        hotel = new Hotel(5, 5);
//        person = new Person();
    }

    public static void main(String[] args) {
//        RoomNumber rn = new RoomNumber();
        Main recepcja = new Main();

        try (Scanner sc = new Scanner(System.in)) {
            int userOptions;
            do {
                recepcja.menuOptions();
                userOptions = sc.nextInt();
                sc.nextLine();
                switch (userOptions) {
                    case 1:
                        recepcja.hotel.printAllRooms();
                        break;
                    case 2:
                        recepcja.hotel.isThereAreFreeRoom();
                        break;
                    case 3:
                        recepcja.hotel.showFreeRoom();
                        break;
                    case 4:
                        recepcja.rentRoomReception(sc);
                        break;
                    case 5:
                        recepcja.hotel.howMuchFreeRooms();
                        break;
                    case 6:
                        recepcja.rentRandomRoomReception(sc);
                        break;
                    case 7:
                        recepcja.isNeighboringRoomsAreFreeReception(sc);
                        break;
                    case 8:
                        recepcja.isGuestRentingRoomReception(sc);
                        break;
                    case 9:
                        recepcja.findRoomsRentedByGuestReception(sc);
                        break;
                    case 10:
                        recepcja.releaseAllGuestRoomsReception(sc);
                        break;
                }
            } while (userOptions != Main.EXIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        hotel.isThereAreFreeRoom();
//        hotel.showFreeRoom();
//        hotel.howMuchFreeRooms();
//        hotel.howMuchFreeRooms();
//        Person person1 = new Person("First", "Last");
//        hotel.rentRandomRoom(person1);
//
//        hotel.rentRandomRoom(new Person("Ola", "mola"));
//        hotel.howMuchFreeRooms();
//        hotel.showFreeRoom();
//        hotel.rentRandomRoom(new Person("Ola", "mola1"));
//        hotel.rentRandomRoom(new Person("Ola", "mol2a"));
//        hotel.rentRandomRoom(new Person("Ola", "mol3a"));
//        hotel.rentRandomRoom(new Person("Ola", "mol4a"));
//        hotel.howMuchFreeRooms();
//        hotel.showFreeRoom();
//        hotel.isNeighboringRoomsAreFree(2, 3);
//
//        Person personForRoom = new Person("Jakub", "Dupa");
//        hotel.isGuestRentingRoom(personForRoom);
//        Person personForRoom2 = new Person("Jakub", "Niedupa");
//        hotel.rentRandomRoom(personForRoom2);
//        hotel.rentRandomRoom(personForRoom2);
//        hotel.isGuestRentingRoom(personForRoom2);
//        hotel.findRoomsRentedByGuest(personForRoom2);
//        hotel.findRoomsRentedByGuest(personForRoom);
//        hotel.showFreeRoom();
//        hotel.releaseAllGuestRooms(personForRoom2);
//        hotel.showFreeRoom();

//        Person kowal = new Person("Jan", "Kowal");
//        hotel.rentRoom(kowal, 3, 2);
//        hotel.rentRoom(kowal, 3, 3);
//        hotel.rentRoom(kowal, 3, 4);
//        hotel.showFreeRoom();
//        Person nowak = new Person("Jacek", "Nowak");
//        hotel.rentRoom(nowak, 3, 3);
//        hotel.findRoomsRentedByGuest(kowal);
//        hotel.releaseAllGuestRooms(kowal);
//        hotel.findRoomsRentedByGuest(kowal);
//        Person smith = new Person("John", "Smith");
//        Person pain = new Person("Rick", "Pain");
//        hotel.showFreeRoom();
////        hotel.rentRandomRoom(smith);
//        hotel.findRoomsRentedByGuest(smith);
//        for (int i = 0; i <= 100; i++) {
//            hotel.rentRandomRoom(smith);
//        }
//        hotel.findRoomsRentedByGuest(smith);
//        hotel.howMuchFreeRooms();
//        hotel.rentRoom(pain, 3, 3);
//        hotel.showFreeRoom();
//
//        hotel.showFreeRoom();
//        hotel.howMuchFreeRooms();
    }

//    private Person person = new Person();

    private void menuOptions() {
        System.out.println("\t" + "***California Hotel v1.0***" + "\n");
        System.out.println("1. Wyświetl wszystkie pokoje" + "\n");
        System.out.println("2. Czy są wolne pokoje" + "\n");
        System.out.println("3. Wyświetl wolne pokoje" + "\n");
        System.out.println("4. Wynajmij pokój" + "\n");
        System.out.println("5. Ile jest wolnych pokoi" + "\n");
        System.out.println("6. Wynajmij dowolny pokój" + "\n");
        System.out.println("7. Czy sąsiednie pokoje są wolne" + "\n");
        System.out.println("8. Czy Gość wynajmuje ktoryś z pokoi" + "\n");
        System.out.println("9. Znajdź pokoje wynajmowane przez Gościa" + "\n");
        System.out.println("10. Zwolnij wszystkie pokoje wynajmowane przez Gościa" + "\n");
}

    private void rentRoomReception(Scanner sc) {
        int floor, room;
        String first, last;

        System.out.println("Podaj imię: ");
        first = sc.nextLine();
        System.out.println("Podaj nazwisko: ");
        last = sc.nextLine();
        System.out.println("Które piętro?");
        floor = sc.nextInt();
        sc.nextLine();
        System.out.println("Który pokój?");
        room = sc.nextInt();
        sc.nextLine();

        hotel.rentRoom(new Person(first, last), floor, room);
    }

    private void rentRandomRoomReception(Scanner sc) {
        String first, last;

        System.out.println("Podaj imię: ");
        first = sc.nextLine();
        System.out.println("Podaj nazwisko: ");
        last = sc.nextLine();

        hotel.rentRandomRoom(new Person(first, last));
    }

    private void isNeighboringRoomsAreFreeReception(Scanner sc) {
        int floor, room;

        System.out.println("Podaj piętro: ");
        floor = sc.nextInt();
        sc.nextLine();
        System.out.println("Podaj pokój: ");
        room = sc.nextInt();
        sc.nextLine();

//        Person person = hotel.roomNumbers[floor][room].getPerson();
        hotel.isNeighboringRoomsAreFree(floor, room);
    }

    private void isGuestRentingRoomReception(Scanner sc) {
        String first, last;

        System.out.println("Podaj imię: ");
        first = sc.nextLine();
//        hotel.person.setFirstName(first);
        person.setFirstName(first);
        System.out.println("Podaj nazwisko: ");
        last = sc.nextLine();
//        hotel.person.setLastName(last);
        person.setLastName(last);
//        hotel.person.setFirstName(first);
//        hotel.person.setLastName(last);
//        person = hotel.person;
//        first = person.getFirstName();
//        last = person.getLastName();

//        person = hotel.roomNumbers[rn.getFloor()][rn.getRoom()].
//        first = person.getFirstName();
//        person.setFirstName(first);
//        person.setLastName(last);
        hotel.isGuestRentingRoom(person);
    }

    private void findRoomsRentedByGuestReception(Scanner sc) {
        String first, last;

        System.out.println("Podaj imię: ");
        first = sc.nextLine();
        System.out.println("Podaj nazwisko: ");
        last = sc.nextLine();

        hotel.findRoomsRentedByGuest(new Person(first, last));
    }

    private void releaseAllGuestRoomsReception(Scanner sc) {
        String first, last;

        System.out.println("Podaj imię: ");
        first = sc.nextLine();
        System.out.println("Podaj nazwisko: ");
        last = sc.nextLine();

        hotel.releaseAllGuestRooms(new Person(first, last));
    }
}
