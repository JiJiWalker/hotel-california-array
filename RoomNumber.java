/*Klasa spełnia zadanie jako klasa zawierająca Pokój oraz Piętro.
* Jej obiekt tworzy jednocześnie piętra oraz pokoje (ograniczona ilość obu wartości)
*
* Pokój jest wynajęty, jeśli powiązana jest z nim klasa Person.
* Obiekt Person może wynajmować wiele pokoi*/


public class RoomNumber {

    private int floor;
    private int room;
    private Person person;

    public RoomNumber() {
    }

    public RoomNumber(int floor, int room) {
        this.floor = floor;
        this.room = room;
        this.person = null;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Piętro: " + floor +
                " Pokój: " + room +"\n";
    }
}
