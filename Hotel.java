/*  Metoda isThereAreFreeRoom: Czy jest jakiś wolny pokój - (true or false. Później można zmienić w showFreeRooms co zwraca wolne pokoje)
*  Metoda howMuchFreeRooms: Ile jest wolnych pokoi
*  Metoda showFreeRooms: Pokaż wolne pokoje;
*  Metoda rentRandomRoom: Wynajmnij dowolny pokój podanej (jako parametr) osobie (czyli nazwaMetody(new Person(konstruktor)). Zwraca numer przydzielonego pokoju
*  Metoda isNeighboringRoomFree: Czy można wynająć 'x' sąsiednich pokoi (z założenia są dwa - po lewej i prawej). Zwraca nr pierwszego pokoju albo null if impossible
*  Metoda isGuestRentingRoom: Czy osoba o podanym nazwisku wynajmuje jakiś pokój
*  Metoda findRoomsRentedByGuest: Które pokoje wynajmuje osoba o podanym nazwisku. Zwraca tablicę numerów pokoi bądź null
*  Metoda releaseAllGuestRooms: Zwolnij wszystkie pokoje wynajmowane przez osobę o podanym nazwisku
*  Metoda rentRoom: Rezerwuje konkretny pokój dla osoby (nazwaMetody(Person), int floor, int room)
* TODO Naprawić nadpisywanie już zajętych pokoi.
* TODO *Specjalna Metoda releaseAllForOne: Zwalnia cały hotel dla jednej osoby.*/


import java.util.Random;


public class Hotel {

    public RoomNumber[][] roomNumbers;
//    public Person person;

    Hotel(int floor, int room) {
        int fl = 0;
        roomNumbers = new RoomNumber[floor][room];
        for (int i = 0; i < roomNumbers.length; i++) {
            fl++;
            for (int j = 0; j < roomNumbers[i].length; j++) {
                RoomNumber flNr = new RoomNumber(fl, j+1);
                roomNumbers[i][j] = flNr;
//                System.out.println(roomNumbers.length);
//                System.out.println(roomNumbers[i].length);
            }
        }
    }

    public RoomNumber[][] getRoomNumbers() {
        return roomNumbers;
    }

    void printAllRooms() {
//        System.out.println(Arrays.deepToString(roomNumbers)); // <--- to nie działa, kiedy nie ma obiketu RoomBumber[][] poza konstruktorem.

        for (RoomNumber[] roomNumber : roomNumbers) {
            for (RoomNumber aRoomNumber : roomNumber) {
                System.out.print(aRoomNumber);
            }
            System.out.println();
        }
    }

    void isThereAreFreeRoom() {
        boolean tmp = false;

        for (int i = 0; i <roomNumbers.length ; i++) {
            for (int j = 0; j <roomNumbers[i].length ; j++) {
                if (roomNumbers[i][j].getPerson() == null) {
                    tmp = true;
                }
            }
        }
        if (tmp) {
            System.out.println("Tak, mamy wolne pokoje."+"\n");
        }
    }

    void showFreeRoom() {
        for (int i = 0; i <roomNumbers.length ; i++) {
            for (int j = 0; j <roomNumbers[i].length ; j++) {
                if (roomNumbers[i][j].getPerson() == null) {
                    System.out.print(roomNumbers[i][j].toString());
                }
            }
            System.out.println();
        }
    }

    void howMuchFreeRooms() {
        int tmp = 0;

        for (int i = 0; i <roomNumbers.length ; i++) {
            for (int j = 0; j <roomNumbers[i].length ; j++) {
                if (roomNumbers[i][j].getPerson() == null) {
                    tmp++;
                }
            }
        }
        System.out.println(tmp + "\n");
    }

    void rentRandomRoom(Person person) {
//        boolean tmp = false;
//        int randomFloor = 0;
//        int randomRoom = 0;
//        zakomentowany kod, dodawał person do tablicy, ale również nadpisywał.
        int randomFloor;
        int randomRoom;

        randomFloor = new Random().nextInt(roomNumbers.length);
        randomRoom = new Random().nextInt(roomNumbers[randomFloor].length);

//        for (int i = 0; i <roomNumbers.length ; i++) {
//            for (int j = 0; j <roomNumbers[i].length ; j++) {
//                if (roomNumbers[i][j].getPerson() == null) {
//                    tmp = true;
//                    randomFloor = new Random().nextInt(roomNumbers.length);
//                    randomRoom = new Random().nextInt(roomNumbers[i].length);
//                }
//            }
//        }
//        if (tmp) {
//            roomNumbers[randomFloor][randomRoom].setPerson(person);
//        }
        if (roomNumbers[randomFloor][randomRoom].getPerson() == null) {
            roomNumbers[randomFloor][randomRoom].setPerson(person);
        }
    }

    void isNeighboringRoomsAreFree(int floor, int room) { // <-- podaje piętro, pokój i czy pokój obok pokoju jest wolny.
        boolean tmp = false;
//        System.out.println(Arrays.deepToString(roomNumbers));
        for (int i = 0; i <roomNumbers.length ; i++) {
            for (int j = 0; j <roomNumbers[i].length ; j++) {
                if (roomNumbers[floor-1][(room-1)-1].getPerson() == null && roomNumbers[floor-1][(room+1)-1].getPerson() == null) {
                    tmp = true;
                }
            }
        }
        if (tmp) {
            System.out.println("Piętro "+floor+" oraz pokój "+room+" z pokojami obok "+(room+1)+" ||| "+(room-1));
            System.out.println(roomNumbers[floor][room].toString());
//            System.out.println(roomNumbers[floor-1][room-1].toString());
            System.out.println("Pokoje " + (room+1) + " i "+ (room-1)+ " na wskazanym piętrze są wolne");
        } else {
            System.out.println("Przynajmniej jeden pokój obok wskazanego jest zajęty.");
        }
    } //zrobione +/-. Parametry metody wskazują nr indeksu, a że pętla liczona jest od 0, to zwraca nr +1

    void isGuestRentingRoom(Person person) {
        boolean tmp = false;

        for (int i = 0; i < roomNumbers.length ; i++) {
            for (int j = 0; j < roomNumbers[i].length ; j++) {
                if (roomNumbers[i][j].getPerson() == person) {
                    tmp = true;
                }
            }
        }
        if (tmp) {
            System.out.println(person.getLastName() + " wynajmuje jakiś pokój.");
        } else {
            System.out.println(person.getLastName() + " nie wynajmuje żadnego pokoju.");
        }
    }

    void findRoomsRentedByGuest(Person person) {
        boolean tmp = false;

        for (int i = 0; i < roomNumbers.length; i++) {
            for (int j = 0; j < roomNumbers[i].length; j++) {
                if (roomNumbers[i][j].getPerson() == person) {
                    tmp = true;
                    System.out.println(person.getLastName() + " wynajmuje pokój: "+roomNumbers[i][j].getRoom() + " na piętrze " + roomNumbers[i][j].getFloor());
//                    System.out.println("Na piętrze: "+roomNumbers[i][j].getFloor());
                }
            }
        }
        if (!tmp) {
            System.out.println(person.getLastName() + " nie wynajmnuje żadnego pokoju.");
        }
    }

    void releaseAllGuestRooms(Person person) {
        for (int i = 0; i < roomNumbers.length; i++) {
            for (int j = 0; j < roomNumbers[i].length; j++) {
                if (roomNumbers[i][j].getPerson() == person) {
                    roomNumbers[i][j].setPerson(null);
                }
            }
        }
    }

    void rentRoom(Person person, int floor, int room) {
//        boolean tmp = false;

//        for (int i = 0; i < roomNumbers.length; i++) {
//            for (int j = 0; j < roomNumbers[i].length; j++) {
//                if (roomNumbers[i][j].getPerson() == null) {
////                    tmp = true;
//                    roomNumbers[floor-1][room-1].setPerson(person);
//                }
//            }
//        }
        if (roomNumbers[floor-1][room-1].getPerson() == null) {
            roomNumbers[floor-1][room-1].setPerson(person);
        } else {
            System.out.println("Ten pokój jest już zajęty");
        }
    }
}
